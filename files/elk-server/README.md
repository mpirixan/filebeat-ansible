# Tutorial de configuração do Elasticsearch e Kibana

O texto abaixo tem como foco guiar a configuração dos servidores após a instalação que irão hospedar os Serviços do Elasticsearch e do Kibana.

## Lista de instruções

## Elasticsearch; [Doc Oficial](https://www.elastic.co/guide/en/elasticsearch/reference/current/configuring-security.html)
1. Acessar a máquina com o elasticsearch;
2. Adicionar a linha 19 no arquivo [elasticsearch.yml](https://gl.idc.ufpa.br/mpirixan/elk-ansible/-/blob/develop/files/elasticsearch/elasticsearch.yml) e restartar o serviço;
3. Acessar como root ou usuário elasticsearch  com sudo;
4.  Executar o commando abaixo, para selecionar as senhas dos usuários padrões; 
```
bin/elasticsearch-setup-passwords interactive
```
5. Restartar o serviço e sair da máquina.

## Kibana;
1. Acessar máquina do Kibana;
2. Adicionar as linhas 25 á 28 no [kibana.yml](https://gl.idc.ufpa.br/mpirixan/elk-ansible/-/blob/develop/files/kibana/kibana.yml) como a seguir;
```
#Pacote de segurança ativado e usuário padrão inbutido
elasticsearch.username: "kibana_system"
elasticsearch.password: "definida_anteriormente"
xpack.security.enabled: true
```
3. Restartar o Serviço;
4. Acessar a interface Web e entrar com o usuário elastic;
5. Criar super usuário com as seguintes roles;
```	
beats-all, ingest_admin, kibana_admin
kibana_system, watcher_admin, transform_admin
superuser, rollup_admin, apm_system
logstash_admin, beats_admin, beats_system
```

Documentações de base:
<p>[Filebeat](https://www.elastic.co/guide/en/beats/filebeat/7.9/index.html);</p>
<p>[Kibana](https://www.elastic.co/guide/en/kibana/7.9/index.html);</p>
<p>[Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/index.html);</p>