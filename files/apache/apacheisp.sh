#!/bin/bash
#Habilita módulo do apache caso esteja fora
#sudo filebeat modules enable apache
#touch /home/sa_idc/sites-isp.txt
ls /var/log/ispconfig/httpd/ >> /home/sa_idc/sites-isp.txt
site="/home/sa_idc/sites-isp.txt"

#Apaga caminhos antigos de sites
sed -i "/ispconfig/d"  /etc/filebeat/modules.d/apache.yml

#Reescreve lista de sites no apache
#rm /home/beats/sites-isp.txt


#Reescreve caminhos atualizados de sites
while IFS= read -r line
do
  echo ""
  echo "Ativando log de acesso de $line"
  sed -i "11 a \      \- /var/log/ispconfig/httpd/$line/*access.log"  /etc/filebeat/modules.d/apache.yml
  echo "Ativando log de erro de $line"
  sed -i "$ a \      \- /var/log/ispconfig/httpd/$line/error.log"  /etc/filebeat/modules.d/apache.yml
done <"$site"

rm /home/sa_idc/sites-isp.txt
#sed -i "/web/d"  /etc/filebeat/modules.d/apache.yml
#sed -i "/ispconfig/d"  /etc/filebeat/modules.d/apache.yml
#sed -i "/clients/d"  /etc/filebeat/modules.d/apache.yml
#sed -i "/conf/d"  /etc/filebeat/modules.d/apache.yml
#sed -i "/apps/d"  /etc/filebeat/modules.d/apache.yml
#sed -i "/php-fcgi-scripts/d"  /etc/filebeat/modules.d/apache.yml
#sed -i "/html/d"  /etc/filebeat/modules.d/apache.yml
#sed -i "/ufpa.br/d"  /etc/filebeat/modules.d/apache.yml
#Reinicia o serviço
#service filebeat restart 