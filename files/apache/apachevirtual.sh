#!/bin/bash
#Habilita módulo do apache caso esteja fora
#sudo filebeat modules enable apache
ls /var/www-virtual/ >> /home/sa_idc/sites-virtual.txt
site="/home/sa_idc/sites-virtual.txt"

#Apaga caminhos antigos de sites
sed -i "/www-virtual/d"  /etc/filebeat/modules.d/apache.yml

#Reescreve lista de sites no apache
#rm /home/sa_idc/sites-virtual.txt


#Reescreve caminhos atualizados de sites
while IFS= read -r line
do
  echo ""
  echo "Ativando log de acesso de $line"
  sed -i "12 a \      \- /var/www-virtual/$line/logs/$line.access_log"  /etc/filebeat/modules.d/apache.yml
  echo "Ativando log de erro de $line"
  sed -i "$ a \      \- /var/www-virtual/$line/logs/$line.error_log"  /etc/filebeat/modules.d/apache.yml
done <"$site"
rm /home/sa_idc/sites-virtual.txt
#Reinicia o serviço
#service filebeat restart 
