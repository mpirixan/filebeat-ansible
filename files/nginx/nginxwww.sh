#!/bin/bash
#Habilita módulo do nginx caso esteja fora
#sudo filebeat modules enable nginx

site="/home/beats/sites.txt"

#Apaga caminhos antigos de sites
sed -i "/www/d"  /etc/filebeat/modules.d/nginx.yml

#Reescreve lista de sites no apache
rm /home/beats/sites.txt
ls /var/www/ >> /home/beats/sites.txt

#Reescreve caminhos atualizados de sites
while IFS= read -r line
do
  echo ""
  echo "Ativando log de acesso de $line"
  sed -i "11 a \      \- /var/www/$line/logs/$line.access_log"  /etc/filebeat/modules.d/nginx.yml
  echo "Ativando log de erro de $line"
  sed -i "$ a \      \- /var/www/$line/logs/$line.error_log"  /etc/filebeat/modules.d/nginx.yml
done <"$site"
