#!/bin/bash
#Habilita módulo do nginx caso esteja fora
#sudo filebeat modules enable nginx

site="/home/beats/sites-virtual.txt"

#Apaga caminhos antigos de sites
sed -i "/www-virtual/d"  /etc/filebeat/modules.d/nginx.yml

#Reescreve lista de sites no apache
rm /home/beats/sites-virtual.txt
ls /var/www-virtual/ >> /home/beats/sites-virtual.txt

#Reescreve caminhos atualizados de sites
while IFS= read -r line
do
  echo ""
  echo "Ativando log de acesso de $line"
  sed -i "11 a \      \- /var/www-virtual/$line/logs/$line.access_log"  /etc/filebeat/modules.d/nginx.yml
  echo "Ativando log de erro de $line"
  sed -i "$ a \      \- /var/www-virtual/$line/logs/$line.error_log"  /etc/filebeat/modules.d/nginx.yml
done <"$site"