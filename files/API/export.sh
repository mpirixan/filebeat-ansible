#!/bin/bash
#Export dashboard
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "dashboard"
}' -v -o dashboard.ndjson
#Export search
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "search"
}' -v -o search.ndjson
#Export index-pattern
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "index-pattern"
}' -v -o index.ndjson
#Export visualization
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "visualization"
}' -v -o visualization.ndjson
#Export url
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "url"
}' -v -o url.ndjson
#Export Maps
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "map"
}' -v -o map.ndjson
#Export lens
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "lens"
}' -v -o lens.ndjson
#Export canvas-workpad
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "canvas-workpad"
}' -v -o canvas-workpad.ndjson
#Export infrastructure-ui-source
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "infrastructure-ui-source"
}' -v -o infrastructure-ui-source.ndjson
#Export canvas-workpad-template
curl -u sa_idc:@Monitoramento -X POST "10.222.1.43:80/api/saved_objects/_export" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
{
  "type": "canvas-workpad-template"
}' -v -o canvas-workpad-template.ndjson
