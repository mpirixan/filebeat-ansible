# ELK-Ansible
 [![pipeline status](https://gl.idc.ufpa.br/mpirixan/elk-ansible/badges/develop/pipeline.svg)](https://gl.idc.ufpa.br/mpirixan/elk-ansible/commits/master)
[![Beats version](https://img.shields.io/badge/Beats-7.9.3-00bfb3?style=flat&logo=beats)](https://www.elastic.co/pt/beats/)

[Tutorial da pipeline](https://drive.google.com/file/d/1FQ3BQpYlP_eMVlPSji4Svt-sp0c1fY2m/view?usp=sharing);

[Tutorial de Busca](https://drive.google.com/file/d/17K4K6nfwqXv4qZK4EFQbIpJXjyN8f-Z4/view?usp=sharing).
# Logs com Filebeat 

O módulo do Filebeat é um agente pertecente da plataforma Beats que coleta e envia os logs dos clientes selecionados para o Elasticsearch, como Apache, MySQL, Nginx, PostgreSQL, Logs do Sistema e demais. Os logs são visualizados no [Kibana](http://10.222.1.43/app/home#/) por meio de Dashboards, tabelas, gráficos e outros recursos visuais.

## Como usar

Para o cliente rodar o Filebeat pelo ansible, é necessário;
1.  O cliente deve ter o usuário padronizado(Entrar em contato com os administradores da pipeline);
2.  Adicionar o dns ou ip da máquina e o tipo de módulo que o cliente irá executar no [inventory.txt](https://gl.idc.ufpa.br/mpirixan/elk-ansible/-/blob/master/inventory.txt);
3.  Caso seja um servidor web, a variável 'Web' definirá os caminhos dos logs no www/ ou www-virtual/: 
Segue exemplo abaixo. 
```
[targets] 
 servidor.ufpa.br          Service="Apache"      Web="Virtual"    typedb="Mysql"

*Obs:Caso queira apenas os logs do sistema no cliente, é só deixar as variáveis em branco.

[targets]
0.0.0.0         Service=""     Web=""    typedb=""
```
4.  Acessar a aba dos index do [Kibana](http://10.222.1.43/app/management/kibana/indexPatterns) com permissões de admin e criar um index pattern relacionado ao novo cliente. Tutorial no [Trac](https://guara.ufpa.br/trac-csi/wiki/config_ELK)

Valores disponíveis para cada variável:
```
[targets] --> Para Debian's >= 8;
[antigos] --> Para Debian's <= 7;
Service --> Apache, Nginx;
Web --> Virtual, WWW;
tybedb --> Mysql, Postgresql.
```
Por padrão, todas irão ter o módulo do sistema para busca de authlog e syslog(mail).

Documentações de base:
<p>[Filebeat](https://www.elastic.co/guide/en/beats/filebeat/7.9/index.html);</p>
<p>[Kibana](https://www.elastic.co/guide/en/kibana/7.9/index.html);</p>
<p>[Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/index.html);</p>
